#!/usr/bin/env bash
set -e

# System dependencies
apt-get update
apt-get install -y curl
rm -rf /var/lib/apt/lists/*
