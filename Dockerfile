FROM docker.io/docker:24.0.7

RUN apk update && \
    apk add --no-cache curl unzip git gcc bash musl-dev

# Install rustup with minimum dependencies
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain stable --profile minimal -y

# Add cargo to $PATH
ENV PATH=/root/.cargo/bin:$PATH

# Install cross
ARG USE_UPSTREAM_CROSS=true
ENV USE_UPSTREAM_CROSS=$USE_UPSTREAM_CROSS
RUN if [ $USE_UPSTREAM_CROSS = "true" ]; then \
      echo "Install cross from GitHub upstream"; \
      cargo install cross --git https://github.com/cross-rs/cross; \
    else \
      echo "Install cross@0.2.5 (latest release at the moment)"; \
      cargo install cross@0.2.5; \
    fi

# Create directories
RUN mkdir -p /work/target

VOLUME /work/target

# Set working directory
WORKDIR /work

# Copy project code
COPY . .
