`cross` in Docker-in-Docker
===========================

Run [`cross`](https://github.com/cross-rs/cross) in Docker-in-Docker configuration.

Build pipeline for GitLAb CI included.

Usage
-----

First, build images: 

```shell
docker-compose build
```

### System docker via socket

Build with `cross-sys` in Docker using local Docker socket:

```shell
docker-compose run --rm cross-sys cross build --bin dummy --target x86_64-unknown-linux-gnu
```

### Remote Docker-in-Docker

Start Docker-in-Docker

```shell
docker-compose up -d docker
```

Build with `cross-remote` which connects to `docker` service and uses remote Docker API:

```shell
docker-compose run --rm cross-remote cross build --bin dummy --target x86_64-unknown-linux-gnu
```
